// syscall.c
// collection of syscalls, i.e., API

#include "spede.h"
#include "syscall.h" 

int GetPid() {
   int pid;

   asm("int $48; movl %%ebx, %0" // CPU inst
       : "=g"  (pid)             // 1 output from asm() 
       :                         // no input into asm()
       : "%ebx");                // push/pop before/after asm()
    
   return pid;
}

void Exit(int n){
	asm("movl %0, %%ebx; int $57"
		:
		: "g" (n)
		: "%ebx");
}

void Fork(char * addr){
	asm(" movl %0, %%ebx; int $55"
		:
		: "g" ((int)addr)
		: "%ebx");
}

void MsgSnd(msg_t * msg){
	asm("movl %0, %%ebx; int $53;"
		:
		:"g" ((int)msg)
		: "%ebx");
}

void MsgRcv(msg_t * msg){
	asm("movl %0, %%ebx; int $54;"
		:
		:"g" ((int)msg)
		: "%ebx");
}

int SemGet(int requested){
	int id;

	asm("movl %1, %%ebx; int $52; movl %%ecx, %0;"
		:"=g" (id)
		:"g" (requested)
		: "%ebx", "%ecx");
	return id;
}

void SemPost(int semaphoreID){
	asm("movl %0, %%ebx; int $51"
		:
		: "g" (semaphoreID)
		: "%ebx");
}

void SemWait(int semaphoreID){
	asm("movl %0, %%ebx; int $50"
		:
		: "g" (semaphoreID)
		: "%ebx");
}

void Sleep(int sec) {
	if(sec <= 0){
		cons_printf("Sleep count is 0");
		breakpoint();
	}else{
		asm("movl %0, %%ebx; int $49"
			:
			: "g" (sec)
			: "%ebx" );
	}
}

void TipIRQ3(){
	// IRQ3_ENTRY
	asm("int $35");
}

int Wait(int * exit_num){
	int ret;

	asm("movl %1, %%ebx; int $56; movl %%ecx, %0"
		: "=g" (ret)
		: "g" ((int)exit_num)
		: "%ebx", "%ecx");
	return ret;
}

