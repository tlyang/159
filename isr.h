// isr.h, 159

#ifndef _ISR_H_
#define _ISR_H_

void CreateISR(int);
void ExitISR();
void ForkISR();
void GetPidISR();
void IRQ3ISR();
void IRQ7ISR();
void MsgSndISR();
void MsgRcvISR();
void SemGetISR();
void SemPostISR();
void SemWaitISR();
void SleepISR();
void TerminateISR();
void TimerISR();
void WaitISR();

#endif
