// type.h, 159

#ifndef _TYPE_H_
#define _TYPE_H_

#include "TF.h"

#define TIME_LIMIT 300         // max timer count to run
#define MAX_PROC 20          // max number of processes
#define Q_SIZE 20            // queuing capacity
#define STACK_SIZE 4096      // process stack in bytes
#define PAGE_SIZE 4096

#define RW_PRESENT 0x03
#define RW 0x01
#define PRES 0x02

// this is the same as constants defined: KMODE=0, UMODE=1
typedef enum {KMODE, UMODE} mode_t;
typedef enum {NONE, RUNNING, RUN, SLEEP, WAIT, ZOMBIE, WAIT_CHILD} state_t;

typedef struct {             // proc queue type
   int head, tail, size;     // where head and tail are, and current size
   int q[Q_SIZE];            // indices into q[] array to place or get element
} q_t;

// PCB
typedef struct {             // PCB describes proc image
   mode_t mode;              // process privilege mode
   state_t state;            // state of process
   int runtime;              // run time since dispatched
   int total_runtime;        // total run time since created
   TF_t *TF_ptr;
   int wake_time;
   int ppid;
   int main_table;
} pcb_t;

// Message Type
typedef struct {
	int sender, recipient, time_stamp, code;
	int number[3];
	char data[101];
} msg_t;

// Message Queue type
typedef struct {
	msg_t msg[Q_SIZE];
	int head, tail, size;
} msg_q_t;

// Message Box
typedef struct {
	msg_q_t msg_q;
	q_t wait_q;
} mbox_t;

// Semaphore
typedef struct {
	int count;
	q_t wait_q[Q_SIZE];
} semaphore_t;

// Terminal type
typedef struct {
	q_t TX_q,		// transmit to terminal 
		RX_q,		// receive from terminal 
		echo_q;		// echo back to terminal
	int TX_sem,		// transmit space available count 
		RX_sem, 	// receive data (arrrived) count
		echo, 		// echo back to terminal (the typing) or not
		TX_extra;	// if 1, TXRDY event occurred but echo_q and TX_q were empty
} terminal_t;

typedef struct {
	int owner;	// PID of the process that is using the page
	int addr;	// Location in the 4KB RAM page
} page_t;

typedef void (* func_ptr_t)();	// void-returning function pointer type

#endif
