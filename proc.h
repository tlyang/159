// proc.h, 159

#ifndef _PROC_H_
#define _PROC_H_

void Consumer();
void FileMgr();
void Idle();
void Init();
void PrintDriver();
void Producer();
void Shell();
void STDIN();
void STDOUT();
void UserProc();

#endif
