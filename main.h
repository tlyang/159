// main.h, 159

#ifndef _MAIN_H_
#define _MAIN_H_

int main();
void InitData();
void InitIDT();
void SelectCRP();
void Kernel();

#endif
