// isr.c, 159

#include "spede.h"
#include "type.h"
#include "isr.h"
#include "tool.h"
#include "extern.h"
#include "proc.h"
#include "TF.h"
#include "FileMgr.h"

/** Helper Methods **********************************************/
void scanSleepQ();
void IRQ3TX();
void IRQ3RX();
void initRegisters(int, unsigned int);
void clearPages(int);

void scanSleepQ(){
	int pid, dq, count;
	
	count = 0;
	while(count < sleep_q.size){
		pid = sleep_q.q[sleep_q.head];
		if( pcb[pid].wake_time == sys_time){
			// Save result for error checking
			dq = DeQ(&sleep_q);
			if(dq != pid){
				cons_printf("\nError dequeueing pid from the sleep_q. Expected %d, Actual %d\n", pid, dq);
				breakpoint();
			}
			EnQ(pid, &run_q);
			pcb[pid].state = RUN;
		}else{
			// Move the pid at head to the end of the queue
			EnQ(DeQ(&sleep_q), &sleep_q);	
		}
		++count;
	}
}

/**
 * Queue char read from port to RX and echo queues
 */
void IRQ3RX(){
	char ch;

	// Use 127 to mask out msb (rest 7 bits in ASCII range)
	// mask 0111 1111
	ch = inportb(COM2_IOBASE+DATA) & 0X7F;

	EnQ(ch, &terminal.RX_q);
	SemPostISR(terminal.RX_sem);

	if(ch == '\r'){
		EnQ('\r', &terminal.echo_q);
		EnQ('\n', &terminal.echo_q);
	}else{
		if(terminal.echo){
			EnQ(ch, &terminal.echo_q);
		}
	}
}

/**
 * Dequeue TX_q to write to port
 */
void IRQ3TX(){
	char ch = 0;

	if(terminal.echo_q.size > 0){
		ch = DeQ(&terminal.echo_q);
	}else{
		if(terminal.TX_q.size > 0){
			ch = DeQ(&terminal.TX_q);
			SemPostISR(terminal.TX_sem);
		}
	}

	if(ch == '\0'){
		terminal.TX_extra = 1;
	}else{
		outportb(COM2_IOBASE+DATA, ch);
		terminal.TX_extra = 0;
	}
}

void clearPages(int owner){
	int i;
	// reclaim the resource page_t
	for(i=0; i<MAX_PROC; i++){
		if(owner == page[i].owner){
			MyBzero((char *)page[i].addr, PAGE_SIZE);
			page[i].owner = -1;
		}
	}
}

/****************************************************************/

void CreateISR(int pid) {
	unsigned int proc;

	if(pid != 0)
		EnQ(pid, &run_q);

	pcb[pid].mode = UMODE;
	pcb[pid].state = RUN;
	pcb[pid].runtime =  0;
	pcb[pid].total_runtime = 0;
	pcb[pid].main_table = sys_main_table;

	// Initialize the trap frame in the stack and point the 
	// pcb of the pid to it.
	MyBzero(stack[pid], STACK_SIZE);

	// Zero out the message box queue for the PID
	MyBzero((char *)mbox[pid].wait_q.q, sizeof(q_t));

	pcb[pid].TF_ptr = (TF_t *)&stack[pid][STACK_SIZE];
	pcb[pid].TF_ptr--;

	// Initialize flags and registers
	pcb[pid].TF_ptr->eflags = EF_DEFAULT_VALUE | EF_INTR;
	pcb[pid].TF_ptr->cs = get_cs();
	pcb[pid].TF_ptr->ds = get_ds();
	pcb[pid].TF_ptr->es = get_es();
	pcb[pid].TF_ptr->fs = get_fs();
	pcb[pid].TF_ptr->gs = get_gs();

	switch(pid){
		case 0:
			proc = (unsigned int)Idle;
			break;
		case 1:
			proc = (unsigned int)Init;
			break;
		case 2:
			proc = (unsigned int)PrintDriver;
			break;
		case 3:
			proc = (unsigned int)Shell;
			break;
		case 4:
			proc = (unsigned int)STDIN;
			break;
		case 5:
			proc = (unsigned int)STDOUT;
			break;
		case 6:
			proc = (unsigned int)FileMgr;
			break;
		default:
			proc = (unsigned int)UserProc;
			break;
	}

	pcb[pid].TF_ptr->eip = proc;
}

void ExitISR(){
	int ppid;
	int child_exit_num;
	int *parent_exit_num_ptr;

	ppid = pcb[CRP].ppid;
	child_exit_num = (int)pcb[CRP].TF_ptr->ebx;

	// If parent has not called Wait() yet...
	if(pcb[ppid].state != WAIT_CHILD){
		pcb[CRP].state = ZOMBIE;
		CRP = -1;
		return;
	}

	set_cr3(pcb[ppid].main_table);
	parent_exit_num_ptr = (int *)pcb[ppid].TF_ptr->ebx;
	*parent_exit_num_ptr = child_exit_num;

	pcb[ppid].TF_ptr->ecx = CRP;
	pcb[ppid].state = RUN;
	EnQ(ppid, &run_q);
	// sys_main_table because CRP is now exiting
	set_cr3(sys_main_table);
	
	clearPages(CRP);

	pcb[CRP].state = NONE;
	EnQ(CRP, &none_q);
	CRP = -1;
}

void ForkISR(){
	int pid;
	int i;
	int page_count = 0;
	int page_index[5];
	int * p;
	int main_table, code_table, stack_table, code_page, stack_page;

	if(none_q.size == 0){
		cons_printf("No more PID available!\n");
		pcb[CRP].TF_ptr->ecx = -1;
		return;
	}

	pid = DeQ(&none_q);

	for(i=0;i<MAX_PROC; i++){
		if(page[i].owner == -1){
			page_index[page_count++] = i;
			if(page_count >= 5) break;
		}
	}

	if(page_count < 5){
		cons_printf("No more RAM available!\n");
		pcb[CRP].TF_ptr->ecx = -1;
		return;
	}

	main_table 	= page_index[0];
	code_table 	= page_index[1];
	stack_table = page_index[2];
	code_page 	= page_index[3];
	stack_page 	= page_index[4];

	// Main table
	//  Save address of code table and page table into entries
	//  512 and 767, respectively. 
	page[main_table].owner = pid;
	MyMemIntcpy(page[main_table].addr, sys_main_table, 16);
	p  = (int *)(page[main_table].addr + 4*512); 	// ints are 32 bits (4 bytes)
	*p = (int)(page[code_table].addr + RW_PRESENT);			// Point to the address
	p  = (int *)(page[main_table].addr + 4*767);
	*p = (int)(page[stack_table].addr + RW_PRESENT);

	// Code table
	//   Save code page address to entry 0
	page[code_table].owner = pid;
	p  = (int *)(page[code_table].addr);
	*p = (int)(page[code_page].addr + RW_PRESENT);

	// Stack table
	//   Save stack page to entry 1023
	page[stack_table].owner = pid;
	p  = (int *)(page[stack_table].addr + 4*1023);
	*p = (int)(page[stack_page].addr + RW_PRESENT);

	// Code page 
	page[code_page].owner = pid;
	MyMemcpy((char *)page[code_page].addr, (char *)pcb[CRP].TF_ptr->ebx, PAGE_SIZE);

	// Stack page, copy executable to addr
	page[stack_page].owner = pid;

	pcb[pid].runtime = 0;
	pcb[pid].total_runtime = 0;
	pcb[pid].state = RUN;
	pcb[pid].mode = UMODE;
	pcb[pid].ppid = CRP;
	pcb[pid].main_table = page[main_table].addr;
	pcb[pid].TF_ptr = (TF_t *)(page[stack_page].addr + PAGE_SIZE - sizeof(TF_t));
	pcb[pid].TF_ptr->eflags = EF_DEFAULT_VALUE | EF_INTR;
	pcb[pid].TF_ptr->cs = get_cs();
	pcb[pid].TF_ptr->ds = get_ds();
	pcb[pid].TF_ptr->es = get_es();
	pcb[pid].TF_ptr->fs = get_fs();
	pcb[pid].TF_ptr->gs = get_gs();
	pcb[pid].TF_ptr->eip = 0x80000080;
	pcb[pid].TF_ptr = (TF_t *)(0xbfffffc0); // TF = 3G - 64

	MyBzero((char *)&mbox[pid].wait_q.q, sizeof(q_t));
	EnQ(pid, &run_q);
}

void WaitISR(){
	int i;
	int child_pid = -1;
	int * parent_exit_num_ptr;
	int child_exit_num;

	for(i=0; i<MAX_PROC; i++){
		if(CRP == pcb[i].ppid && pcb[i].state == ZOMBIE){
			child_pid = i;
			break;
		}
	}

	if(child_pid == -1){
		pcb[CRP].state = WAIT_CHILD;
		CRP = -1;
		return;
	}else{
		pcb[CRP].TF_ptr->ecx = child_pid;	// Put into ecx for Wait() to return it
		parent_exit_num_ptr = (int *)pcb[CRP].TF_ptr->ebx;

		set_cr3(pcb[child_pid].main_table);
		child_exit_num = pcb[child_pid].TF_ptr->ebx;
		set_cr3(sys_main_table);

		*parent_exit_num_ptr = child_exit_num;
	}

	clearPages(child_pid);
	pcb[child_pid].state = NONE;
	EnQ(child_pid, &none_q);
}

void GetPidISR(){
	// GetPID() will retrieve from %ebx
	pcb[CRP].TF_ptr->ebx = CRP;
}

void IRQ3ISR(){
	int event;

	// Dismiss IRQ3
	outportb(0x20, 0x63);

	event = inportb(COM2_IOBASE+IIR);
	switch(event){
		case IIR_TXRDY:
			IRQ3TX();
			break;
		case IIR_RXRDY:
			IRQ3RX();
			break;
	}

	if(terminal.TX_extra){
		IRQ3TX();
	}
}

void IRQ7ISR(){
	// Dismiss IRQ7
	outportb(0x20, 0x67);

	SemPostISR(print_semaphore);
}

void MsgSndISR(){
	//	The data in ebx is of type int (casted from syscall)
	//		We need to cast to a msg_t pointer then dereference it
	msg_t sending_msg = *((msg_t *)pcb[CRP].TF_ptr->ebx);
	msg_t *dest_msg;
	int pid;

	sending_msg.sender = CRP;
	sending_msg.time_stamp = sys_time;


	if(mbox[sending_msg.recipient].wait_q.size == 0){
		MsgEnQ(&sending_msg, &mbox[sending_msg.recipient].msg_q);
	}else{
		// Release recipient and copy message over
		pid = DeQ(&mbox[sending_msg.recipient].wait_q);
		set_cr3(pcb[pid].main_table);
		EnQ(pid, &run_q);
		pcb[pid].state = RUN;
		dest_msg = (msg_t *)pcb[pid].TF_ptr->ebx;
		*dest_msg = sending_msg;
		set_cr3(pcb[CRP].main_table);
	}
}

void MsgRcvISR(){
	msg_t *crp_msg = (msg_t *)pcb[CRP].TF_ptr->ebx;
	msg_t *msg;

	if(mbox[CRP].msg_q.size == 0){
		// Block CRP
		EnQ(CRP, &(mbox[CRP].wait_q));
		pcb[CRP].state = WAIT;
		CRP = -1;
	}else{
		msg = MsgDeQ(&mbox[CRP].msg_q);
		*crp_msg = *msg;
	}
}

void SemGetISR(){
	int semID;
	
	if(semaphore_q.size == 0){
		return;
	}

	semID = DeQ(&semaphore_q);
	
	if(semID >= 0){
		MyBzero((char *)semaphore[semID].wait_q, sizeof(q_t));
	}
	semaphore[semID].count = pcb[CRP].TF_ptr->ebx;
	pcb[CRP].TF_ptr->ecx = semID;
}

void SemWaitISR(){
	int semID = pcb[CRP].TF_ptr->ebx;

	if(semaphore[semID].count > 0) 
		semaphore[semID].count--;
	else{
		EnQ(CRP, semaphore[semID].wait_q);
		pcb[CRP].state = WAIT;
		CRP = -1;
	}
}

void SemPostISR(int id){
	int semID = id;
	int pid;

	if((semaphore[semID].wait_q)->size == 0) 
		semaphore[semID].count++;
	else{
		pid = DeQ(semaphore[semID].wait_q);
		EnQ(pid, &run_q);
		pcb[pid].state = RUN;
	}
}

void SleepISR(){
	int sec = pcb[CRP].TF_ptr->ebx;

	pcb[CRP].wake_time = sys_time + (100 * sec);
	EnQ(CRP, &sleep_q);
	pcb[CRP].state = SLEEP;
	CRP = -1;
}

void TerminateISR() {
	if(CRP <= 0)
		return;
	pcb[CRP].state = NONE;
	EnQ(CRP, &none_q);
	CRP = -1;
}        

void TimerISR() {
	sys_time++;
	// Scan sleep_q for sleeping processes that need to wake up
	scanSleepQ();

	// Dismiss timer intr (IRQ 0), otherwise new intr signal from timer
	// won't be recognized by CPU since circuit uses edge-trigger flipflop
	outportb(0x20, 0x60);

	if(CRP <= 0)
		return;

	pcb[CRP].runtime++;

	// Reset the CRP since it has reached its time limit.
	if(pcb[CRP].runtime == TIME_LIMIT){
		pcb[CRP].total_runtime += pcb[CRP].runtime;
		pcb[CRP].runtime = 0;
		pcb[CRP].state = RUN;
		EnQ(CRP, &run_q);
		CRP = -1;
	}
}

