// tool.c, 159

#include "spede.h"
#include "type.h"
#include "extern.h"
#include "tool.h"

void MyBzero(char *p, int size) {
	int i;
	for(i=0; i < size; i++){
		*(p+i) = 0;
	}
}

void EnQ(int pid, q_t *p) {
	if(p->size == MAX_PROC)
		return;

	p->q[p->tail] = pid;
	p->tail = ++(p->tail) % Q_SIZE;
	++(p->size);
}

int DeQ(q_t *p) {
	int pid;

	if(p->size == 0)
		return -1;

	pid = p->q[p->head];
	p->head = ++(p->head) % Q_SIZE;
	--(p->size);

	return pid;
}

void MsgEnQ(msg_t *p, msg_q_t *q){
	if(q->size == MAX_PROC){
		cons_printf("\nMessage queue is full!");
		return;
	}

	q->msg[q->tail] = *p;
	q->tail = ++(q->tail) % Q_SIZE;
	++(q->size);
}

msg_t * MsgDeQ(msg_q_t *p){
	msg_t *dqd_msg;

	if(p->size == 0){
		cons_printf("Message queue is empty.  Cannot DeQ!");
		return 0;
	}

	dqd_msg = &(p->msg[p->head]);
	p->head = ++(p->head) % Q_SIZE;
	--(p->size);

	return dqd_msg;
}

void MyStrcpy(char * dest, char * src){
	while(*src != '\0'){
		*dest++ = *src++;
	}
	// null-terminated
	*dest = '\0';
}

int MyStrlen(char * s){
	int len;
	for(len=0; s[len]; len++);
	return len;
}

int MyStrcmp(char * s1, char * s2){
	if(MyStrlen(s1) != MyStrlen(s2)){
		return 0;
	}

	while(*s1 != '\0'){
		if(*s1++ != *s2++){
			return 0;
		}
	}
	return 1;
}

void MyMemcpy(char *dest, char *src, int size){
	int i;

	for(i = 0; i < size; i++){
		*(dest+i) = *(src+i);
	}
}

void MyMemIntcpy(int dest_addr, int src_addr, int size){
	int i;

	for(i = 0; i < size; i++){
		*(int*)(dest_addr+i) = *(int*)(src_addr+i);
	}
}

int MyStrcmpSize(char * s1, char * s2, int size){
	int i;

	for(i = 0; i< size; i++){
		if(*(s1+i) != *(s2+i))
			return 0;
	}
	return 1;
}

