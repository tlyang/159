//
// Team Name: 	TE 
// Members:		yangt (Thomas Yang) 
// Phase 9:  Virtual Memory Paging System
// main.c, 159
//

#include "spede.h"      // spede stuff
#include "main.h"       // main stuff
#include "isr.h"        // ISR's
#include "tool.h"       // handy functions
#include "proc.h"       // processes such as Init()
#include "type.h"       // processes such as Init()
#include "TF.h"
#include "entry.h"		// Dispatch()


// kernel data structure:
int CRP;
int sys_time;		
int print_semaphore;
char stack[MAX_PROC][STACK_SIZE];
int sys_main_table;

terminal_t terminal;

mbox_t mbox[MAX_PROC];
pcb_t pcb[MAX_PROC];    		
semaphore_t semaphore[Q_SIZE];

struct i386_gate *IDT_ptr;

//Queues
q_t run_q;
q_t none_q;
q_t semaphore_q;
q_t sleep_q;

page_t page[MAX_PROC];

void SetEntry(int entry_num, func_ptr_t func_ptr){
	struct i386_gate *gateptr = &IDT_ptr[entry_num];
	fill_gate(gateptr, (int)func_ptr, get_cs(), ACC_INTR_GATE, 0);
}

int main() {
	InitData();
	InitIDT();
	CreateISR(0);
	Dispatch(pcb[0].TF_ptr);
	return 0;
}

void InitIDT(){
	IDT_ptr = get_idt_base();

	// prime IDT entries
	SetEntry(32, TimerEntry);	
	SetEntry(35, IRQ3Entry);
	SetEntry(39, IRQ7Entry);
	SetEntry(48, GetPidEntry);
	SetEntry(49, SleepEntry);
	SetEntry(50, SemWaitEntry);
	SetEntry(51, SemPostEntry);
	SetEntry(52, SemGetEntry);
	SetEntry(53, MsgSndEntry);
	SetEntry(54, MsgRcvEntry);
	SetEntry(55, ForkEntry);
	SetEntry(56, WaitEntry);
	SetEntry(57, ExitEntry);

	// Initialize Hardware Interrupts
	outportb(0x21, ~0x89);	// program pic mask = (128 + 1 + 8) = 137 = 0x89
}

void InitData() {
	int i;
	sys_time = 0;

	// sys_main_table holds the original system main
	//   translation table
	sys_main_table = get_cr3();
	
	// Initialize Queues
	MyBzero((char *)run_q.q, sizeof(q_t));
	MyBzero((char *)none_q.q, sizeof(q_t));
	MyBzero((char *)sleep_q.q, sizeof(q_t));
	MyBzero((char *)semaphore_q.q, sizeof(q_t));

	for(i = 1; i < Q_SIZE; i++){
		EnQ(i, &none_q);
		EnQ(i, &semaphore_q);
	}
	
	for(i = 0; i < MAX_PROC; i++){
		pcb[i].state = NONE;
		page[i].owner = -1;
		page[i].addr = 0xE00000 + 0x1000 * i;	// 14M + 4096 * index
												// base + (size * index)
	}

	/**
	 * Initialize the starting processes: 
	 * 	Idle, Init, PrintDriver
	 *
	 *	DeQ the first two PIDs (1 & 2) to CreateISR
	 *	to have the two initial pids ready for 1:Init()
	 *	and 2:PrintDriver()
	 */
	CreateISR(DeQ(&none_q));	// 1
	CreateISR(DeQ(&none_q));	// 2
	CreateISR(DeQ(&none_q));	// 3
	CreateISR(DeQ(&none_q));	// 4
	CreateISR(DeQ(&none_q));	// 5
	CreateISR(DeQ(&none_q));	// 6

	CRP = 0;
}

void SelectCRP() {       // select which PID to be new CRP
	if(CRP > 0)
		return;

	if(CRP == 0)
		pcb[CRP].state = RUN;

	if(run_q.size == 0) 
		CRP = 0;
	else
		CRP = DeQ(&run_q);
	
	pcb[CRP].mode = UMODE;
	pcb[CRP].state = RUNNING;
}

void Kernel(TF_t *TF_ptr) {
	pcb[CRP].mode = KMODE;
	pcb[CRP].TF_ptr = TF_ptr;

	switch(TF_ptr->intr_num){
		case GETPID_INTR:
			GetPidISR();
			break;
		case IRQ3_INTR:
			IRQ3ISR();
			break;
		case IRQ7_INTR:
			IRQ7ISR();
			break;
		case SEMGET_INTR:
			SemGetISR();
			break;
		case SEMPOST_INTR:
			SemPostISR(TF_ptr->ebx);
			break;
		case SEMWAIT_INTR:
			SemWaitISR();
			break;
		case SLEEP_INTR:
			SleepISR();
			break;
		case TIMER_INTR:
			TimerISR();	
			break;
		case MSGSND_INTR:
			MsgSndISR();
			break;
		case MSGRCV_INTR:
			MsgRcvISR();	
			break;
		case FORK_INTR:
			ForkISR();
			break;
		case WAIT_INTR:
			WaitISR();
			break;
		case EXIT_INTR:
			ExitISR();
			break;
		default:
			cons_printf("Error in TF_ptr!");
			breakpoint();
			break;
	}

	SelectCRP();
	// Update system main table to
	//   set the MMU to start using the 
	//   CRP's main table
	set_cr3(pcb[CRP].main_table);
	Dispatch(pcb[CRP].TF_ptr);
}

