// tool.h, 159

#ifndef _TOOL_H
#define _TOOL_H

#include "type.h" // q_t needs be defined in code below

int DeQ(q_t *);

void EnQ(int, q_t *);

msg_t * MsgDeQ(msg_q_t *);

void MsgEnQ(msg_t *, msg_q_t *);

void MyBzero(char* , int);

void MyMemcpy(char *, char *, int);
void MyMemIntcpy(int, int, int);

/**
 * Params:
 * char * dest
 * char * src
 */
void MyStrcpy(char *, char *);

int MyStrcmp(char *, char *);

int MyStrcmpSize(char *, char *, int);

int MyStrlen(char *);

#endif

