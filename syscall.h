// syscall.h

#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "type.h"

int GetPid();      // no input, 1 return
void Exit(int);
void Fork(char *);
int SemGet(int);
void MsgSnd(msg_t *);
void MsgRcv(msg_t *);
void SemPost(int);
void SemWait(int);
void Sleep(int);   // 1 input, no return
void TipIRQ3();
int Wait(int *);

#endif
