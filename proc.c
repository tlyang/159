// proc.c, 159
// processes are here

#include "spede.h"   // for IO_DELAY() needed here below
#include "extern.h"  // for current_run_pid needed here below
#include "proc.h"    // for Idle, SimpleProc, DispatchProc
#include "syscall.h"
#include "tool.h"
#include "FileMgr.h"

#define SLEEP_CONST 4
#define PROD_INCR 	100
#define PRINT_PID 	2
#define SHELL_PID	3
#define	STDIN_PID  4			// PIDs of these processes
#define STDOUT_PID 5
#define FILE_MGR_PID 6

void sendMsgTo(msg_t *, int, char *);
void sendMsgFromTo(msg_t *, int, char *, int);
void ShellDir(char *, int, int);
void ShellDirStr(attr_t *, char *);
int applyProtocol(msg_t *, char *, int);

void sendMsgFromTo(msg_t * msg,  int recipient, char * message, int sender){
	MyStrcpy(msg->data, message);
	msg->recipient = recipient;
	msg->sender = sender;
	MsgSnd(msg);
}

void ShellTyp(char * cmd, int STDOUT, int FileMgr){
	char * correct_usage = "Usage: typ [path] <filename>\r\n";
	char obj[101];
	char str[101];
	attr_t *p;
	msg_t msg;
	
	MyStrcpy(obj, cmd+4);

	p = (attr_t *)msg.data;

	if(applyProtocol(&msg, obj, CHK_OBJ) != GOOD || A_ISDIR(p->mode)){
		sendMsgFromTo(&msg, STDOUT, correct_usage, SHELL_PID);
		MsgRcv(&msg);
		return;
	}

	if(applyProtocol(&msg, obj, OPEN_OBJ) == GOOD){
		while(applyProtocol(&msg, obj, READ_OBJ) == GOOD){
			MyStrcpy(str, msg.data);
			sendMsgFromTo(&msg, STDOUT, str, SHELL_PID);
			MsgRcv(&msg);
		}
	}		
	if(applyProtocol(&msg, obj, CLOSE_OBJ) != GOOD){
		sendMsgFromTo(&msg, STDOUT, "Could not close File!\r\0", SHELL_PID);
		MsgRcv(&msg);
	}
}

void ShellDir(char * cmd, int STDOUT, int FileMgr){
	char obj[101];
	char str[101];
	msg_t msg;
	attr_t * p;

	// If cmd is "dir\0" (or "333\0") assume root: "dir/\0"
	// else, there should be an obj after 1st 4 letters "dir "
	if(MyStrcmp(cmd, "dir\0") || MyStrcmp(cmd, "333\0")){
		obj[0] = '/';
		obj[1] = '\0';
	}else{
		cmd += 4;
		MyStrcpy(obj, cmd);
	}
	
	if( applyProtocol(&msg, obj, CHK_OBJ) == BAD){	//Bad protocol returned
		sendMsgFromTo(&msg, STDOUT, "Error in FileMgr!\0", SHELL_PID);
		MsgRcv(&msg);
		return;
	}

	p = (attr_t *)msg.data;
	if( !A_ISDIR(p->mode) ){
		ShellDirStr(p, str);
		msg.code = READ_OBJ;
		sendMsgFromTo(&msg, STDOUT, str, SHELL_PID);
		MsgRcv(&msg);
		return;
	}

	if(applyProtocol(&msg, obj, OPEN_OBJ) == GOOD){
		while(applyProtocol(&msg, obj, READ_OBJ) == GOOD){
			p = (attr_t *)msg.data;
			ShellDirStr(p, str);
			sendMsgFromTo(&msg, STDOUT, str, SHELL_PID);
			MsgRcv(&msg);
		}
	}		
	if(applyProtocol(&msg, obj, CLOSE_OBJ) != GOOD){
		sendMsgFromTo(&msg, STDOUT, "Could not close File!\r\0", SHELL_PID);
		MsgRcv(&msg);
	}
}

int applyProtocol(msg_t *msg, char * obj, int protocol){
	msg->code = protocol;
	msg->sender = SHELL_PID;
	sendMsgTo(msg, FILE_MGR_PID, obj);
	MsgRcv(msg);
	return msg->code;
}

void ShellDirStr(attr_t *p, char *str){
	// p points to attr_t and then obj name (p+1)
	char *obj = (char *)(p+1);

	// make str from the attr_t that p points to
	sprintf(str, " - - - -  size = %6d	%s\r\n", p->size, obj);
	if( A_ISDIR(p->mode) )str[1] = 'd';				// mode is directory
	if( QBIT_ON(p->mode, A_ROTH) ) str[3] = 'r';	// mode is readable
	if( QBIT_ON(p->mode, A_WOTH) ) str[5] = 'w';	// mode is writable 
	if( QBIT_ON(p->mode, A_XOTH) ) str[7] = 'x';	// mode is executable 
}


void Consumer(){
	int i;

	while(1){
		for(i = 0; i < 1666000; i++) IO_DELAY(); 
	}
}

void Idle() {
	int i;

	while(1){
		cons_printf("0 ");
		for(i = 0; i < 1666000; i++) IO_DELAY();
	}
}

void Init(){
	int key;
	msg_t msg;
	char str[] = "Hello, my team is TE!\n\0";
	msg.recipient = PRINT_PID;	// Printdriver's PID (2)
	MyStrcpy(msg.data, str);

	while(1){
		cons_printf("%d ", CRP);
		Sleep(1);

		if(cons_kbhit()){
			key = cons_getchar();
			switch(key){
				case 'b':
					breakpoint();
					break;
				case 'q':
					exit(0);
					break;
				case 'p':
					MsgSnd(&msg);
					break;
			}
		}
	}
}

void PrintDriver(){
	int i, code;
	char * p;
	msg_t msg;

	print_semaphore = SemGet(-1);

	// Printer Control, SeLeCT INterrupt
	outportb(LPT1_BASE+LPT_CONTROL, PC_SLCTIN);
	code = inportb(LPT1_BASE+LPT_STATUS);
	//Needs delay
	for(i=0; i<100000; i++) IO_DELAY();
	//IRQ ENable
	outportb(LPT1_BASE+LPT_CONTROL, PC_INIT|PC_SLCTIN|PC_IRQEN);
	//Printer needs time to reset
	Sleep(1);

	while(1){
		cons_printf("%d ", CRP);

		MsgRcv(&msg);
		p = msg.data; 

		while(*p != '\0'){
			// Send char (*p) to data reg
			outportb(LPT1_BASE+LPT_DATA, *p);
			// Read control reg
			code = inportb(LPT1_BASE+LPT_CONTROL);
			// Send with added strobe
			outportb(LPT1_BASE+LPT_CONTROL, code|PC_STROBE);
			// Delay for EPSON LP-571 printer
			//
			// **NOTE: the original loop limit did not see
			// 			to be long enough for the printer to 
			// 			recover, thus increasing to 50000
			for(i=0; i<5000; i++); IO_DELAY();
			// Send original control code
			outportb(LPT1_BASE+LPT_CONTROL, code);

			SemWait(print_semaphore);

			p++;
		}
	}
}

void Producer(){
	int i;

	while(1){
		for(i = 0; i < 1666000; i++) IO_DELAY(); 
	}
}

void Shell(){
	char * prompt = "\r\n\nValid Commands: whoami, bye (for now), dir, typ \r\n\0";
	char * login_prompt = "TE> login: \0";
	char * pw_prompt = "TE> password: \0";
	char * login_error = "Invalid login!\r\n\0";
	char * shell_prompt = "TE> \0";
	char * invalid_cmds = "Command not found!\r\n\0";

	int BAUD_RATE, divisor;	// for serial port
	char login[101];		// login and password strings 
	char password[101];		// 

	char buf[60];
	int child_pid;
	int exit_num;
	msg_t msg;
	attr_t * p;

	// Initialize interface data structure and serial port
	MyBzero((char *)terminal.TX_q.q, sizeof(q_t)); 
	MyBzero((char *)terminal.RX_q.q, sizeof(q_t)); 
	MyBzero((char *)terminal.echo_q.q, sizeof(q_t)); 

	terminal.TX_sem = SemGet(Q_SIZE); 
	terminal.RX_sem = SemGet(0);
	terminal.echo = 1;
	terminal.TX_extra = 1;

	/*
	 COM1-8_IOBASE: 0x3f8 0x2f8 0x3e8 0x2e8 0x2f0 0x3e0 0x2e0 0x260
	 transmit speed 9600 bauds, clear IER, start TXRDY and RXRDY
	 Data communication acronyms:
		IIR : Intr Indicator Reg
		IER : Intr Enable Reg
		ETXRDY : Enable Xmit Ready
		ERXRDY : Enable Recv Ready
		MSR : Modem Status Reg
		MCR : Modem Control Reg
		LSR : Line Status Reg
		CFCR : Char Format Control Reg
		LSR_TSRE : Line Status Reg, Xmit+Shift Regs Empty
	*/
	
	BAUD_RATE = 9600;
	divisor = 115200/BAUD_RATE;
	outportb(COM2_IOBASE+CFCR, CFCR_DLAB);
	outportb(COM2_IOBASE+BAUDLO, LOBYTE(divisor));
	outportb(COM2_IOBASE+BAUDHI, HIBYTE(divisor));
	// set CFCR: 7-E-1 (7 data bits, even parity, 1 stop bit)
	outportb(COM2_IOBASE+CFCR, CFCR_PEVEN|CFCR_PENAB|CFCR_7BITS);
	outportb(COM2_IOBASE+IER, 0);
	// raise DTR, RTS of the serial port to start read/write
	outportb(COM2_IOBASE+MCR, MCR_DTR|MCR_RTS|MCR_IENABLE);
	IO_DELAY();
	outportb(COM2_IOBASE+IER, IER_ERXRDY|IER_ETXRDY);	//enable TX,RX events
	IO_DELAY();
	// End initialization
	
	while(1){
		while(1){
			// STDOUT for commands
			sendMsgTo(&msg, STDOUT_PID, prompt);
			MsgRcv(&msg);

			// STDOUT for login name
			sendMsgTo(&msg, STDOUT_PID, login_prompt);
			MsgRcv(&msg);

			// STDIN to get login
			sendMsgTo(&msg, STDIN_PID, "\0");
			MsgRcv(&msg);
			MyStrcpy(login, msg.data);
			
			// prompt for password
			sendMsgTo(&msg, STDOUT_PID, pw_prompt);
			MsgRcv(&msg);

			// get password
			sendMsgTo(&msg, STDIN_PID, "\0");
			MsgRcv(&msg);
			MyStrcpy(password, msg.data);

			// if(password entered == login, break loop
			// else prompt "Invalid login!\n\0"
			if(MyStrcmp(login, password)){
				break;
			}else{
				sendMsgTo(&msg, STDOUT_PID, login_error);
				MsgRcv(&msg);
			}
		}
		
		// Get the commands
		while(1){
			sendMsgTo(&msg, STDOUT_PID, shell_prompt);
			MsgRcv(&msg);
			sendMsgTo(&msg, STDIN_PID, "\0");
			MsgRcv(&msg);
			
			if(MyStrlen(msg.data) == 0){
				continue;
			}else if(MyStrcmp(msg.data, "bye")){
				break;
			}else if(MyStrcmp(msg.data, "whoami")){
				sendMsgTo(&msg, STDOUT_PID, login);
				MsgRcv(&msg);
				sendMsgTo(&msg, STDOUT_PID, "\r\n\0");
				MsgRcv(&msg);
				continue;
			}else if(MyStrcmpSize(msg.data, "dir", 3)){
				ShellDir(msg.data, STDOUT_PID, FILE_MGR_PID);
			}else if(MyStrcmpSize(msg.data, "typ", 3)){
				ShellTyp(msg.data, STDOUT_PID, FILE_MGR_PID);
			}else{
				p = (attr_t *)msg.data;
				if(applyProtocol(&msg, msg.data, CHK_OBJ) != GOOD || 
					p->mode != MODE_EXEC){

					sendMsgTo(&msg, STDOUT_PID, invalid_cmds);

					MsgRcv(&msg);

				}else if(p->mode == MODE_EXEC){
					// Mode is executable, so fork a process to execute
					// and wait for it to return.
					// p->data is the addr of the executable
					Fork(p->data);	

					child_pid = Wait(&exit_num);
					
					sprintf(buf, "Child PID: %d\tExit Num: %d\n", child_pid, exit_num);

					sendMsgTo(&msg, STDOUT_PID, buf);
					MsgRcv(&msg);
				}
				continue;
			}
		}
	}
}

void sendMsgTo(msg_t * msg, int recipient, char * message){
	MyStrcpy(msg->data, message);
	msg->recipient = recipient;
	MsgSnd(msg);
}

void STDIN(){
	msg_t msg;
	char * p;
	char ch;

	while(1){
		MsgRcv(&msg);
		p = msg.data;

		while(1){
			SemWait(terminal.RX_sem);
			ch = DeQ(&terminal.RX_q);

			if(ch == '\r') break;

			*p++ = ch;
		}

		*p = '\0';

		msg.recipient = msg.sender;
		msg.sender = SHELL_PID;
		MsgSnd(&msg);
	}
}

void STDOUT(){
	msg_t msg;
	char * p;
	char ch;

	while(1){
		MsgRcv(&msg);	
		p = msg.data;

		while((ch = *p++) != '\0'){
			SemWait(terminal.TX_sem);
			EnQ(ch, &terminal.TX_q);

			TipIRQ3();	//Signal to IRQ3 to start

			if(ch == '\n'){
				SemWait(terminal.TX_sem);
				EnQ('\r', &terminal.TX_q);
			}
		}

		msg.recipient = msg.sender;
		MsgSnd(&msg);
	}
}

void UserProc() {
	while(1){
		cons_printf("%d ", GetPid());
		Sleep(SLEEP_CONST - (GetPid() % SLEEP_CONST));
	}
}
