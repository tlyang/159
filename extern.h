// extern.h, 159

#ifndef _EXTERN_H_
#define _EXTERN_H_

#include "type.h"            // q_t, pcb_t, MAX_PROC, STACK_SIZE

extern int CRP;              // PID of currently-running process, -1 means none
extern int sys_time;
extern int print_semaphore;
extern char stack[MAX_PROC][STACK_SIZE]; // proc run-time stacks

// Queues of types
extern mbox_t mbox[MAX_PROC];	// Message box queue
extern pcb_t pcb[MAX_PROC]; 	// process table
extern semaphore_t semaphore[Q_SIZE];	// Queue of semaphores

//Queues
extern q_t run_q;
extern q_t none_q;
extern q_t semaphore_q;
extern q_t sleep_q;

extern page_t page[MAX_PROC];

extern terminal_t terminal;

extern int sys_main_table;

#endif
