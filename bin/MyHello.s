# MyHello.s
#
# Only from Phase 8 with Real Addressing:
# 	in order to perform syscall MsgSnd(&msg), MsgRcv(&msg), need to
#  	first calculate where the real RAM addr of my msg is:
#  	use ESP - 4096 to get the base of the 4KB RAM page this program resides.
#  	Since $msg is virtual, at 2G+x (0x80000000+x), and the page is virtual
# 	at 2G (0x80000000); subtract 2G from $msg gets x. Add x to the base is
#  	the real RAM addr of the msg.

.text                       # code segment
.global _start              # _start is main()

_start:                     # instructions begin
   #copy $msg to register ebx to use as parameter
   movl $msg, %ebx

   #MsgSnd(&msg)
   int $53

   #MsgRcv(&msg)
   int $54

   #pop to ecx (get a copy, real msg addr)
   popl %ecx

   #copy time stamp (base ecx + offset of time stamp) to ebx
   movl 8(%ecx), %ebx

   #call interrupt number 57  
   int $57

.data                       # data segment follows code segment in RAM
msg:                        # my msg
    .long 0 	      	        # msg.sender
    .long 5                  # msg.recipient
    .long 0                  # msg.time_stamp
	.long 0
	.rept 3
		.long 0
	.endr
    .ascii "Hello from TE!\n"          # msg.data
	.rept 86
		.ascii "\0"
	.endr

